import React from "react";
import Workouts from "../../components/Workouts";

const WorkoutsPage = (props) => {
  return (
    <div>
      <Workouts workouts={props.workouts} />
    </div>
  );
};

export async function getStaticProps() {
  const res = await fetch(process.env.GCP_DB_WORKOUTS);
  const workouts = await res.json();
  /*
{
"comments": "quarantine. home. work. 16-00",
"creationDate": "",
"day": "WEDNESDAY",
"exercises": "",
"month": "JUNE",
"record": "",
"sets": "",
"time": "",
"type": "DELTS",
"week": "",
"workoutDate": "06/23/2021"
}
  */
  return { props: { workouts } };
}

export async function getStaticPaths() {
  // Call an external API endpoint to get posts
  const res = await fetch(process.env.GCP_DB_WORKOUTS);
  const workouts = await res.json();

  // Get the paths we want to pre-render based on posts
  const paths = workouts.map((w) => ({
    params: { id: w.workoutDate },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false };
}

export default WorkoutsPage;
