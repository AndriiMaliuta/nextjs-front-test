import styles from "../styles/Home.module.css";

export default function Home(props) {
  console.log(props);

  // const [dogs, setDogs] = useState([]);
  // const [loaded, setLoaded] = false;

  // useEffect(() => {}, []);

  return (
    <div className={styles.container}>
      <h2>Home</h2>
      <div>TEST</div>
    </div>
  );
}

export async function getStaticProps(context) {
  // console.log(process.env.AGE);

  // const dogs = await fetch('http://localhost:8080/rest/api/v1/dogs')
  //   .then((res) => res.json())
  //   .catch((err) => console.log(err));

  // console.log(dogs);

  return {
    props: {
      dogs: [],
    },
  };
}
