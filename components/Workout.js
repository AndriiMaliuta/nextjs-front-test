import React from "react";
import styles from "../styles/Workouts.module.css";

const Workout = ({ workout }) => {
  return (
    <div className={styles.workout}>
      Workout
      <div>{workout.workoutDate}</div>
      <div>{workout.type}</div>
      <div>{workout.day}</div>
      <div>{workout.month}</div>
      <div>{workout.week}</div>
      <div>{workout.record}</div>
      <div>{workout.sets}</div>
      <div>{workout.time}</div>
      <div>{workout.exercises}</div>
      <div>{workout.comments}</div>
    </div>
  );
};

export default Workout;
