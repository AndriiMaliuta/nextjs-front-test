import React from 'react';
import Nav from './Nav';
import Meta from './Meta';

const Layout = ({ children }) => {
  return (
    <div>
      <Meta />
      <Nav />
      {children}
    </div>
  );
};

export default Layout;
