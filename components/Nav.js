import styles from '../styles/Nav.module.css';
import Link from 'next/link';

const Nav = () => {
  return (
    <div className={styles.nav}>
      <div className={styles.navElem}>
        <Link href='/'>Home</Link>
      </div>
      <div className={styles.navElem}>
        <Link href='/cats'>Cats</Link>
      </div>
      <div className={styles.navElem}>
        <a href='/api/auth/login'>Login</a>
      </div>
      <div className={styles.navElem}>
        <a href='/api/auth/logout'>Logout</a>
      </div>
    </div>
  );
};

export default Nav;
