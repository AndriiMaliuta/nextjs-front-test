import React from "react";
import Workout from "./Workout";
import styles from "../styles/Workouts.module.css";

const Workouts = ({ workouts }) => {
  return (
    <div className={styles.workouts}>
      {workouts.map((w) => (
        <Workout workout={w} />
      ))}
    </div>
  );
};

export default Workouts;
